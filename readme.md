## Ejercicio 1:

1. Creamos una nueva rama llamada bibliografia\
El comando que hemos utilizado es:  **git branch -d _(Nombre de la rama)_**\
\
![Imagen_1](./Capturas/imagen1.png "Imagen1")


## Ejercicio 2:

1. Creamos una nueva página web llamada capitulo4.html\
Pondremos como parrafo:\
**En este capítulo veremos cómo usar GitLab para alojar repositorios en remoto.**

![Imagen_2](./Capturas/imagen2.png "Imagen2")


2. Lo que acanamos de hacer lo subimos al Staging Area.\
Con el siguiente comando:\
**git add**

![Imagen_3](./Capturas/imagen3.png "Imagen3")

3. Realizamos un commit:\
**git commit -m "TEXTO A INTRODUCIR"**

![Imagen_4](./Capturas/imagen4.png "Imagen4")

4. Mostramos el historial del respositorio incluyendo todas las ramas, de forma gráfica:\
**git log --graph --all --oneline**

![Imagen_5](./Capturas/imagen5.png "Imagen5")

## Ejercicio 3:

1. Cambiamos a la rama bibliografia:\
**git checkout bibliografia**

![Imagen_6](./Capturas/imagen6.png "Imagen6")

2. Creamos una página web en el directorio capitulos llamada bibliografia.hmtl:\
Añadimos el siguiente texto: Chacon, S. And Straub, B. Pro Git. Apress\
  
   **git checkout bibliografia**

![Imagen_7](./Capturas/imagen7.png "Imagen7")

3. Añadimos los cambios al Staging Area.\
  
   **git add .**

![Imagen_8](./Capturas/imagen8.png "Imagen8")

4. Realizamos un commit con el mensaje.\
   "Añadida primera referencia bibliográfica"
   **git commit -m "TEXTO"**

![Imagen_9](./Capturas/imagen9.png "Imagen9")

4. Realizamos un commit con el mensaje.\
   "Añadida primera referencia bibliográfica"
   **git commit -m "TEXTO"**

![Imagen_9](./Capturas/imagen9.png "Imagen9")


5. Mostar el historial del repositorio incluyendo todas las ramas. Lenguajes de marcas y sistemas de gestión de la información Tema 8.
   **git log --graph --all --oneline**

![Imagen_10](./Capturas/imagen10.png "Imagen10")

## Ejercicio 4:

1. Funcionamos la rama bibliografia con la rama master.\
**git checkout master**

![Imagen_11](./Capturas/imagen11.png "Imagen11")
![Imagen_12](./Capturas/imagen12.png "Imagen12")

2. Mostrar el historial del repositorio incluyendo todas las ramas.\
**git log --graph --all --oneline**
 
![Imagen_13](./Capturas/imagen13.png "Imagen13")

3. Eliminar la rama bibliografia\
**git branch -d bibliografia**
 
![Imagen_14](./Capturas/imagen14.png "Imagen14")


4. Mostrar de nuevo el historial del repositorio incluyendo todas las ramas.\
**git log --graph --all --oneline**
 
![Imagen_15](./Capturas/imagen15.png "Imagen15")



## Ejercicio 5

1. Crea la rama bibliografia.\
**git branch bibliografia**
 
![Imagen_16](./Capturas/imagen16.png "Imagen16")

2. Cambia a la rama bibliografia.\
**git checkout bibliografia**
 
![Imagen_17](./Capturas/imagen17.png "Imagen17")

3. Cambiar el fichero bibliografia.html para que contenga las siguientes referencias:\

• Chacon, S. And Straub, B. Pro Git. Apress

• Ryan Hodson. Ry`s Git Tutorial. Smashwords (2014)

**git checkout bibliografia**
 
![Imagen_18](./Capturas/imagen18.png "Imagen18")

4. Añadir los cambios al Staging Area y hacer un commit con el mensaje “Añadida nueva referencia bibliográfica”.\
**git add .**
**git commit -m "Añadida nueva referencia bibliografica"**
 
![Imagen_19](./Capturas/imagen19.png "Imagen19")

5. Cambia a la rama master.\
**git checkout master**
 
![Imagen_20](./Capturas/imagen20.png "Imagen20")

6. Cambiar el fichero bibliografia.html para que contenga las siguientes referencias:

• Chacon, S. And Straub, B. Pro Git. Apress

• Loeliger, J. and McCullough, M Version control with Git. O’Reilly\
 
![Imagen_21](./Capturas/imagen21.png "Imagen21")


7. Añadir los cambios al Staging Area y hacer un commit con el mensaje “Añadida nueva referencia bibliográfica”\
 
![Imagen_22](./Capturas/imagen22.png "Imagen22")

8. Fusionamos la rama bibliografia con la master\

**git merge bibliografia**
 
![Imagen_23](./Capturas/imagen23.png "Imagen23")

9. Resolver el conflicto dejando el fichero bibliografía.html con las referencias:

• Chacon, S. And Straub, B. Pro Git. Apress

• Loeliger, J. and McCullough, M Version control with Git. O’Reilly

• Hodson, R. Ry`s Git Tutorial. Smashwords (2014)

 
![Imagen_24](./Capturas/imagen24.png "Imagen24")


10. Añadir los cambios al Staging Area y hacer un commit con el mensaje “Resuelto conflicto de bibliografía”

**git add .**
**git commit -m "TEXTO"**

 
![Imagen_25](./Capturas/imagen25.png "Imagen25")

11. Mostrar el historial del repositorio incluyendo todas las ramas.

**git log --graph --all --oneline**

![Imagen_26](./Capturas/imagen26.png "Imagen26")















